From: Andrej Shadura <andrew.shadura@collabora.co.uk>
Date: Thu, 10 Nov 2022 15:36:13 +0100
Subject: Consistently use deb_chroot instead of chroot where it makes a difference

Signed-off-by: Andrej Shadura <andrew.shadura@collabora.co.uk>
---
 build-pkg-deb            |  6 +++---
 build-recipe-debootstrap |  8 ++++----
 build-recipe-dsc         | 24 ++++++++++++------------
 3 files changed, 19 insertions(+), 19 deletions(-)

diff --git a/build-pkg-deb b/build-pkg-deb
index 9467f46..e18b5be 100644
--- a/build-pkg-deb
+++ b/build-pkg-deb
@@ -71,14 +71,14 @@ pkg_initdb_deb() {
 	cp $BUILD_ROOT/.init_b_cache/rpms/dpkg.deb $BUILD_ROOT/.init_b_cache/dpkg.deb || cleanup_and_exit 1
     fi
     DEB_UNSAFE_IO=
-    chroot $BUILD_ROOT dpkg --force-unsafe-io --version >/dev/null 2>&1 && DEB_UNSAFE_IO="--force-unsafe-io"
+    deb_chroot $BUILD_ROOT dpkg --force-unsafe-io --version >/dev/null 2>&1 && DEB_UNSAFE_IO="--force-unsafe-io"
     deb_chroot $BUILD_ROOT dpkg --install $DEB_UNSAFE_IO --force-depends .init_b_cache/dpkg.deb >/dev/null 2>&1
 }
 
 pkg_prepare_deb() {
     # test if dpkg knows --force-unsafe-io
     DEB_UNSAFE_IO=
-    chroot $BUILD_ROOT dpkg --force-unsafe-io --version >/dev/null 2>&1 && DEB_UNSAFE_IO="--force-unsafe-io"
+    deb_chroot $BUILD_ROOT dpkg --force-unsafe-io --version >/dev/null 2>&1 && DEB_UNSAFE_IO="--force-unsafe-io"
 }
 
 pkg_install_deb() {
@@ -104,7 +104,7 @@ pkg_erase_deb() {
       while read line; do
           case "$line" in
               subprocess\ installed\ *script\ returned\ error\ exit\ status*)
-                 chroot $BUILD_ROOT rm -f /var/lib/dpkg/info/$PKG.{pre,post}rm
+                 deb_chroot $BUILD_ROOT rm -f /var/lib/dpkg/info/$PKG.{pre,post}rm
                  retry=1
               ;;
               *) echo "$line" ;;
diff --git a/build-recipe-debootstrap b/build-recipe-debootstrap
index 064be36..a5585d6 100644
--- a/build-recipe-debootstrap
+++ b/build-recipe-debootstrap
@@ -53,14 +53,14 @@ recipe_build_debootstrap() {
     done
     FULL_PKG_LIST="${FULL_PKG_LIST#,}"
     rm -rf "$BUILD_ROOT/$myroot"
-    if chroot $BUILD_ROOT dpkg --force-unsafe-io --version >/dev/null 2>&1 ; then
+    if deb_chroot $BUILD_ROOT dpkg --force-unsafe-io --version >/dev/null 2>&1 ; then
 	mkdir -p "$BUILD_ROOT/$myroot/etc/dpkg/dpkg.cfg.d"
 	echo force-unsafe-io > "$BUILD_ROOT/$myroot/etc/dpkg/dpkg.cfg.d/force-unsafe-io"
     fi
 
-    set -- chroot $BUILD_ROOT debootstrap --keep-debootstrap-dir --no-check-gpg --variant=buildd --arch="${arch}" --include="$FULL_PKG_LIST" "$dist" "$myroot" file:///.build.binaries
+    set -- deb_chroot $BUILD_ROOT debootstrap --keep-debootstrap-dir --no-check-gpg --variant=buildd --arch="${arch}" --include="$FULL_PKG_LIST" "$dist" "$myroot" file:///.build.binaries
     echo "running debootstrap:$(printf ' %q' "$@")..."
-    if ! "$@" || ! chroot $BUILD_ROOT dpkg --configure -a; then
+    if ! "$@" || ! deb_chroot $BUILD_ROOT dpkg --configure -a; then
         cat $BUILD_ROOT/$myroot/debootstrap/debootstrap.log
         cleanup_and_exit 1 "Failed to setup debootstrap chroot"
     fi
@@ -91,7 +91,7 @@ recipe_build_debootstrap() {
     mkdir -p $BUILD_ROOT/$myroot/dev/shm
     mount -n -ttmpfs none $BUILD_ROOT/$myroot/dev/shm
 
-    chroot $BUILD_ROOT/$myroot apt-cache gencaches
+    deb_chroot $BUILD_ROOT/$myroot apt-cache gencaches
 
     if ! test -e $BUILD_ROOT/$myroot/etc/hosts ; then
         cp $BUILD_ROOT/etc/hosts $BUILD_ROOT/$myroot/etc/hosts
diff --git a/build-recipe-dsc b/build-recipe-dsc
index b6f998b..a37a3fb 100644
--- a/build-recipe-dsc
+++ b/build-recipe-dsc
@@ -67,7 +67,7 @@ recipe_prepare_dsc() {
 	for DEB_DSCFILE in $BUILD_ROOT/$DEB_SOURCEDIR/*.dsc ; do : ; done 
 	DEB_DSCFILE="${DEB_DSCFILE##*/}"
     fi
-    chroot $BUILD_ROOT su -c "dpkg-source -x $DEB_SOURCEDIR/$DEB_DSCFILE $TOPDIR/BUILD" - $BUILD_USER
+    deb_chroot $BUILD_ROOT su -c "dpkg-source -x $DEB_SOURCEDIR/$DEB_DSCFILE $TOPDIR/BUILD" - $BUILD_USER
 
     # Alternative to debtransform: apply OBS release number if tag OBS-DCH-RELEASE is set.
     if test -z "$DEB_TRANSFORM" -a -n "$DEB_RELEASE"; then
@@ -76,7 +76,7 @@ recipe_prepare_dsc() {
 	else
 	        OBS_DCH_RELEASE="$DEB_RELEASE"
 	fi
-        chroot $BUILD_ROOT su -c /bin/sh <<EOF
+        deb_chroot $BUILD_ROOT su -c /bin/sh <<EOF
 cd $TOPDIR/BUILD
 [ ! -f debian/changelog ] && exit 0
 # avoid devscripts dependency and mimic dch
@@ -111,22 +111,22 @@ dsc_build() {
 
     echo "==== Filesystems in build chroot ===="
     echo
-    chroot "$buildroot" su -c "cat /proc/self/mountinfo" - "$BUILD_USER" < /dev/null || true
+    deb_chroot "$buildroot" su -c "cat /proc/self/mountinfo" - "$BUILD_USER" < /dev/null || true
     echo
 
     echo "==== User identity in build chroot ===="
     echo
-    chroot "$buildroot" su -c "id" - "$BUILD_USER" < /dev/null || true
+    deb_chroot "$buildroot" su -c "id" - "$BUILD_USER" < /dev/null || true
     echo
 
     echo "==== Environment variables in build chroot ===="
     echo
-    chroot "$buildroot" su -c "env | LC_ALL=C sort -u" - "$BUILD_USER" < /dev/null || true
+    deb_chroot "$buildroot" su -c "env | LC_ALL=C sort -u" - "$BUILD_USER" < /dev/null || true
     echo
 
     echo "==== Packages installed in build chroot ===="
     echo
-    chroot "$buildroot" su -c "dpkg-query -W" - "$BUILD_USER" < /dev/null || true
+    deb_chroot "$buildroot" su -c "dpkg-query -W" - "$BUILD_USER" < /dev/null || true
     echo
 
     DSC_BUILD_OPTIONS=
@@ -144,7 +144,7 @@ dsc_build() {
 	chmod +x $buildroot/$TOPDIR/SOURCES/build.script
     fi
 
-    chroot $buildroot su -c "export DEB_BUILD_OPTIONS=${DSC_BUILD_OPTIONS} ; cd $TOPDIR/BUILD && $DSC_BUILD_CMD" - $BUILD_USER < /dev/null && BUILD_SUCCEEDED=true
+    deb_chroot $buildroot su -c "export DEB_BUILD_OPTIONS=${DSC_BUILD_OPTIONS} ; cd $TOPDIR/BUILD && $DSC_BUILD_CMD" - $BUILD_USER < /dev/null && BUILD_SUCCEEDED=true
 
     for changes in "$buildroot/$TOPDIR"/*.changes ; do
 	echo
@@ -168,17 +168,17 @@ dsc_build() {
 		(*.deb|*.ddeb|*.udeb)
 		    echo "==== $f ===="
 		    echo
-		    chroot "$buildroot" su -c "dpkg-deb --info $(printf '%q' "$TOPDIR/$f")" - "$BUILD_USER" < /dev/null || true
+		    deb_chroot "$buildroot" su -c "dpkg-deb --info $(printf '%q' "$TOPDIR/$f")" - "$BUILD_USER" < /dev/null || true
 		    echo
-		    chroot "$buildroot" su -c "dpkg-deb --contents $(printf '%q' "$TOPDIR/$f")" - "$BUILD_USER" < /dev/null || true
+		    deb_chroot "$buildroot" su -c "dpkg-deb --contents $(printf '%q' "$TOPDIR/$f")" - "$BUILD_USER" < /dev/null || true
 		    echo
 	    esac
 	done < <(sed -ne '/Files:/,$s/^ ................................ [0-9][0-9]* [^ ]* [^ ]* //p' "$changes")
     done
 
-    if test "$BUILD_SUCCEEDED" = true -a "$DO_CHECKS" != "false" && ( chroot $buildroot su -c "which lintian > /dev/null" - $BUILD_USER < /dev/null ); then
-	DEB_CHANGESFILE=${DEB_DSCFILE%.dsc}$OBS_DCH_RELEASE"_"$(chroot $buildroot su -c 'dpkg-architecture -qDEB_BUILD_ARCH')".changes"
-	chroot $buildroot su -c "cd $TOPDIR && echo Running lintian && (set -x && lintian -i $TOPDIR/$DEB_CHANGESFILE)" - $BUILD_USER < /dev/null || BUILD_SUCCEEDED=false
+    if test "$BUILD_SUCCEEDED" = true -a "$DO_CHECKS" != "false" && ( deb_chroot $buildroot su -c "which lintian > /dev/null" - $BUILD_USER < /dev/null ); then
+	DEB_CHANGESFILE=${DEB_DSCFILE%.dsc}$OBS_DCH_RELEASE"_"$(deb_chroot $buildroot su -c 'dpkg-architecture -qDEB_BUILD_ARCH')".changes"
+	deb_chroot $buildroot su -c "cd $TOPDIR && echo Running lintian && (set -x && lintian -i $TOPDIR/$DEB_CHANGESFILE)" - $BUILD_USER < /dev/null || BUILD_SUCCEEDED=false
     fi
 }
 
